"use strict";

const btnSet = document.querySelectorAll ('.btn');

document.body.addEventListener ('keydown', function (event) {
  btnSet.forEach (function (elem) {
    if (event.key.toLowerCase () === elem.textContent.toLowerCase () ) {
        elem.style.backgroundColor = 'blue';
    } else {
        elem.style.backgroundColor = 'black';
    }
  })
})

